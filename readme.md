# Hackernewsbot

A simple script to read hackernews top stories and report to telegram

## Quickstart

1. copy config_example.py to config.py

2. modify config.py with your telegram bot token and chatids to push updates to

3. make setup

4. make run

## Docker Quickstart

1. copy config_example.py to config.py

2. modify config.py with your telegram bot token and chatids to push updates to

3. make dockerbuild

4. make dockerrun

