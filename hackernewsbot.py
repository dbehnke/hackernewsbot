import sys
import sqlite3
from typing import List
import time
import requests
import telegram
import config
import feedparser
import hashlib
from twitter_scraper import get_tweets

def hackernews(maxresults: int) -> List[str]:
    messages = []
    conn = sqlite3.connect('hackernews.db')

    c = conn.cursor()

    # Create table
    c.execute('''CREATE TABLE IF NOT EXISTS seen
                (id number primary key, seentimestamp number)''')

    purgetime = time.time() - 86400
    c.execute('''DELETE FROM seen
                 where seentimestamp < ?''', (purgetime,))
    conn.commit()

    r = requests.get("https://hacker-news.firebaseio.com/v0/topstories.json?print=pretty")

    articles = r.json()

    articlecount = 0
    for article in articles:
        articlecount = articlecount + 1
        if articlecount > maxresults:
            break
        c.execute('SELECT id from seen where id = ?', (article,))
        row = c.fetchone()

        if row is not None:
            continue
        print("new article: {}".format(article))
        c.execute('INSERT into seen values (?,?)', (article, time.time(),))
        conn.commit()

        r = requests.get("https://hacker-news.firebaseio.com/v0/item/{}.json?print=pretty"
                         .format(article))
        data = r.json()
        title = data['title']
        url = data['url'] if 'url' in data else ""
        hackernewsurl = ("https://news.ycombinator.com/item?id={}"
                         .format(article))
        messagetext = "{} - Article: {} - Comments: {}".format(title, url, hackernewsurl)
        print(data)
        print(messagetext)
        messages.append(messagetext)

    conn.close()
    return messages


def twitter(accountname: str, maxresults:int = 10) -> List[str]:
    tweeturl_format = 'https://twitter.com/{}/status/{}'
    message_format = '🐦 [{}] {} status link: {}'
    tweets = get_tweets(accountname, pages=1)

    conn = sqlite3.connect('hackernews.db')

    c = conn.cursor()

    # Create table
    c.execute('''CREATE TABLE IF NOT EXISTS twitter_seen
                (id string primary key, seentimestamp number)''')
    purgetime = time.time() - (86400 * 30) # 30 days
    c.execute('''DELETE FROM twitter_seen
                 where seentimestamp < ?''', (purgetime,))
    conn.commit()

    i = 0
    messages = []
    for tweet in tweets:
        if i > maxresults:
            break
        tweetId = tweet['tweetId']
        c.execute('SELECT id from twitter_seen where id = ?', (tweetId,))
        row = c.fetchone()

        if row is not None:
            continue
        
        c.execute('INSERT into twitter_seen values (?,?)', (tweetId, time.time(),))
        conn.commit()

        tweeturl = tweeturl_format.format(accountname, tweetId)
        message = message_format.format(accountname, tweet['text'], tweeturl)
        messages.append(message)
    return messages

def reddit(maxresults: int) -> List[str]:
    useragent = "daffybot-" + str(time.time())
    r = requests.get("https://www.reddit.com/.json?feed={}"
                     .format(config.REDDIT_FEED),
                     headers={'User-agent': useragent})
    data = r.json()
    posts = data['data']['children']
    messages = []
    conn = sqlite3.connect('hackernews.db')

    c = conn.cursor()

    # Create table
    c.execute('''CREATE TABLE IF NOT EXISTS reddit_seen
                (id string primary key, seentimestamp number)''')
    purgetime = time.time() - 86400
    c.execute('''DELETE FROM reddit_seen
                 where seentimestamp < ?''', (purgetime,))
    conn.commit()

    postcount = 0
    for post in posts:
        postcount = postcount + 1
        if postcount > maxresults:
            break
        data = post['data']
        postid = data['id']
        c.execute('SELECT id from reddit_seen where id = ?', (postid,))
        row = c.fetchone()

        if row is not None:
            continue
        c.execute('INSERT into reddit_seen values (?,?)', (postid, time.time(),))
        conn.commit()

        message = "{} - https://reddit.com{}".format(
            data['title'], data['permalink'])
        messages.append(message)
        print(message)
    return messages

def slashdot() -> List[str]:
    messages = []
    conn = sqlite3.connect('hackernews.db')

    c = conn.cursor()

    # Create table
    c.execute('''CREATE TABLE IF NOT EXISTS slashdot_seen
                (id text primary key, seentimestamp number)''')

    purgetime = time.time() - 86400
    c.execute('''DELETE FROM slashdot_seen
                 where seentimestamp < ?''', (purgetime,))
    conn.commit()

    d = feedparser.parse('http://rss.slashdot.org/Slashdot/slashdotMain')
    
    for entry in d['entries']:
        entry_title = entry['title']
        entry_link = entry['links'][0]['href'].split('?utm_source')[0]
        m = hashlib.sha256()
        m.update(bytes(entry_title,'UTF-8'))
        entry_id = m.hexdigest()
        c.execute('SELECT id from slashdot_seen where id = ?', (entry_id,))
        row = c.fetchone()

        if row is not None:
            continue

        c.execute('INSERT into slashdot_seen values (?,?)', (entry_id, time.time(),))
        conn.commit()
        messagetext = "/. {} - {}".format(entry_title, entry_link)
        print(messagetext)

        messages.append(messagetext)
    
    return(messages)

def telegrambot(messages: List[str]):
    bot = telegram.Bot(token=config.BOT_TOKEN)
    # updates = bot.get_updates()
    #limit = 15
    #messages_sent = 0
    #for u in updates:
    #    print(u)
    for m in messages:
        print(m)
        #messages_sent = messages_sent + 1
        for id in config.BOT_CHATS:
            bot.send_message(id, m)
        time.sleep(10)
        #if messages_sent >= limit:
        #    print("Message per minute limit reached (%d), sleeping 60 seconds" % limit)
        #    time.sleep(60)
        #    messages_sent = 0

def main():
    while 1:
        try:
            messages = []
            try:
                print("Checking HackerNews")
                hackernews_messages = hackernews(10)
                messages = messages + hackernews_messages
            except:
                print("HackerNews Error:", sys.exc_info()[0])
            
            try:
                print("Checking reddit")
                reddit_messages = reddit(10)
                messages = messages + reddit_messages
            except:
                print("Reddit Error:", sys.exc_info()[0])

            try:
                print("Checking slashdot")
                slashdot_messages = slashdot()
                messages = messages + slashdot_messages
            except:
                print("Slashdot Error:", sys.exc_info()[0])
        
            print("Checking Twitter")
            twitter_accounts = ('realdonaldtrump','reuters','gvanrossum',
                                'oraclelinux','virtualbox','golang')
            for twitter_account in twitter_accounts:
                print("Checking @{}".format(twitter_account))
                try:
                    twitter_messages = twitter(twitter_account)
                    print("  {} new tweets".format(len(twitter_messages)))
                    messages = messages + twitter_messages
                except Exception as e:
                    print("  error:", e)
            print("Sending Telegram Messages",len(messages))
            print(messages)
            try:
                telegrambot(messages)
            except Exception as e:
                print("Error sending", e)
            print("Sleeping")
            time.sleep(120)
        except KeyboardInterrupt:
            raise
        except:
            print("Unexpected error:", sys.exc_info()[0])

if __name__ == "__main__":
    main()
    # messages = twitter('realdonaldtrump')
    # print(messages)
    
